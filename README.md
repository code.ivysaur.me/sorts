# sorts

![](https://img.shields.io/badge/written%20in-Javascript-blue)

A graphical visualisation of sorting algorithms.

- Options for bubblesort, quicksort, heapsort, and mergesort
- All sorts are in-place
- You can randomly permute the input data, apply a minor perturbation, or reverse it

## Changelog

2012-05-08
- File timestamp


## Download

- [⬇️ sorts.zip](dist-archive/sorts.zip) *(2.20 KiB)*
- [⬇️ sorts-combined.htm](dist-archive/sorts-combined.htm) *(6.14 KiB)*
